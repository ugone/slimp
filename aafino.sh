#!/bin/bash
#
#exec 5> /home/pi/debug_output.txt
#BASH_XTRACEFD="5"
#PS4='$LINENO: '
#set -x
#controlare di aver installato mencoder
#creare una cartella chiamata nuvole (o quel che si vuole e modificare lo script di conseguenza)
#si sposta nella cartella di lavoro
cd /home/pi/nuvole/
#crea cartella con nome data e ci entra dentro
mkdir -p "$(date +%F)" && cd "$_"
#la camera registra foto dove -t da la durata in ms  e --timelapse quanto passa tra un fotogramma e l'altro
#
#libcamera-still -t 3600000 -o %05d.jpg --timelapse 7000 ## 1 ora e 7 sec tra una foto e la succ
#libcamera-still -t 7200000 -o %05d.jpg --timelapse 7000 ## 2 ore e 7 sec tra una foto e la succ
#libcamera-still -t 10800000 -o %05d.jpg --timelapse 7000 ## 3 ore e 7 sec tra una foto e la succ
#libcamera-still -t 14400000 -o %05d.jpg --timelapse 7000 ## 4 ore e 7 sec tra una foto e la succ
#libcamera-still -t 18000000 -o %05d.jpg --timelapse 7000 ## 5 ore e 7 sec tra una foto e la succ
#libcamera-still -t 21600000 -o %05d.jpg --timelapse 7000 ## 6 ore e 7 sec tra una foto e la succ
#libcamera-still -t 25200000 -o %05d.jpg --timelapse 7000 ## 7 ore e 7 sec tra una foto e la succ
#libcamera-still -t 28800000 -o %05d.jpg --timelapse 7000 ## 8 ore e 7 sec tra una foto e la succ
#libcamera-still -t 32400000 -o %05d.jpg --timelapse 7000 ## 9 ore e 7 sec tra una foto e la succ
#libcamera-still -t 36000000 -o %05d.jpg --timelapse 7000 ## 10 ore e 7 sec tra una foto e la succ
#libcamera-still -t 39600000 -o %05d.jpg --timelapse 7000 ## 11 ore e 7 sec tra una foto e la succ
libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000 ## 12 ore e 7 sec tra una foto e la succ
#libcamera-still -t 46800000 -o %05d.jpg --timelapse 7000 ## 13 ore e 7 sec tra una foto e la succ
#libcamera-still -t 50400000 -o %05d.jpg --timelapse 7000 ## 14 ore e 7 sec tra una foto e la succ
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#quando ha finito la registrazione crea un video
mencoder "mf://*.jpg" -mf fps=25:type=jpg -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell:vbitrate=7000 -vf scale=640:480 -oac copy -o movie$(date -I).avi
#Aspetta 60 sec e rimuove tutti i file.jpg dalla cartella
sleep 60
rm *.jpg
